<?php

namespace Training\Test\App;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterListInterface;
use Magento\Framework\App\Request\ValidatorInterface as RequestValidator;
use Magento\Framework\Message\ManagerInterface as MessageManager;
use Psr\Log\LoggerInterface;

class FrontController extends \Magento\Framework\App\FrontController
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * FrontController constructor.
     * @param RouterListInterface $routerList
     * @param ResponseInterface $response
     * @param LoggerInterface $logger
     * @param RequestValidator|null $requestValidator
     * @param MessageManager|null $messageManager
     */
    public function __construct(
        RouterListInterface $routerList,
        ResponseInterface $response,
        LoggerInterface $logger,
        RequestValidator $requestValidator = null,
        MessageManager $messageManager = null
    ) {
        $this->logger = $logger;

        parent::__construct($routerList, $response, $requestValidator, $messageManager, $logger);
    }

    /**
     * @param \Magento\Framework\App\RequestInterface $request
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        foreach ($this->_routerList as $router) {
            $this->logger->info(get_class($router));
        }
        return parent::dispatch($request);
    }
}