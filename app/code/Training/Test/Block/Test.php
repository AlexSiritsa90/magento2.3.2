<?php

namespace Training\Test\Block;

class Test extends \Magento\Framework\View\Element\AbstractBlock
{
    protected function _toHtml()
    {
        $message = "<b>" . __('Hello world from block!') . "</b>";
        return $message;
    }
}