<?php

namespace Training\Test\Block\Product\View;

class Description extends \Magento\Catalog\Block\Product\View\Description
{
    protected function _toHtml()
    {
        $text = __('Test description');
        return $text;
    }
}