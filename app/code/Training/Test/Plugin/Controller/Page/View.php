<?php

namespace Training\Test\Plugin\Controller\Page;

use \Magento\Framework\Exception\NoSuchEntityException;

class View
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    protected $pageRepository;

    /**
     * @var \Magento\Cms\Api\PageRepositoryInterface
     */
    protected $request;

    /**
     * View constructor.
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Cms\Api\PageRepositoryInterface $pageRepository
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository
    ) {
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->pageRepository = $pageRepository;
    }

    public function afterExecute(
        \Magento\Cms\Controller\Page\View $subject,
        $result
    )
    {
        if (!$this->request->isAjax()) {
            return $result;
        }

        $data = [
            'status' => 'success',
            'message' => ''
        ];

        $pageId = $this->request->getParam('page_id', $this->request->getParam('id', false));
        $resultJson = $this->resultJsonFactory->create();
        try {
            $page = $this->pageRepository->getById($pageId);
            $data['title'] = $page->getTitle();
            $data['content'] = $page->getContent();
        } catch (NoSuchEntityException $e) {
            $data['status'] = 'error';
            $data['message'] = __('Not found');
        } catch (\Exception $e) {
            $data['status'] = 'error';
            $data['message'] = __('Something wrong');
        }

        $resultJson->setData($data);
        return $resultJson;
    }
}