<?php

namespace Training\Test\Plugin\Block;

class Template
{
    public function afterToHtml(
        \Magento\Framework\View\Element\Template $subject,
        $result
    )
    {
        if ($subject->getNameInLayout() !== 'top.search') {
            return $result;
        }

        $newResult = '<hr>';
        $newResult .= '<div><p>';
        $newResult .= $subject->getTemplate();
        $newResult .= '</p><p>';
        $newResult .= get_class($subject);
        $newResult .= '</p>';
        $newResult .= $result;
        $newResult .= '</div>';

        return $newResult;
    }
}