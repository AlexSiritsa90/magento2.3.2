<?php

namespace Training\TestObjectManager\Model;

class Manager implements ManagerInterface
{
    public function create()
    {
        // something create
    }

    public function get()
    {
        // something get
    }
}