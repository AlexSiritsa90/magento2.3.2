<?php

namespace Training\TestObjectManager\Model;

class ManagerCustomImplementation implements ManagerInterface
{
    public function create()
    {
        // something create
    }

    public function get()
    {
        // something get
    }
}