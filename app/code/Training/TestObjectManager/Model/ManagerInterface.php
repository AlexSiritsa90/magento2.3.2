<?php

namespace Training\TestObjectManager\Model;

interface ManagerInterface
{
    public function create();

    public function get();
}