<?php

namespace Training\TestObjectManager\Model;

class PlayWithTest
{
    private $testObject;
    private $testObjectFactory;
    private $manager;

    public function __construct(
        \Training\TestObjectManager\Model\Test $testObject,
        \Training\TestObjectManager\Model\TestFactory $testObjectFactory,
        \Training\TestObjectManager\Model\ManagerCustomImplementation $manager
    )
    {
        $this->testObject = $testObject;
        $this->testObjectFactory = $testObjectFactory;
        $this->manager = $manager;
    }

    public function run()
    {
        $this->testObject->log();

        $customArrayList = [
            'item' => 'a1b2c3',
            'item2' => '777',
            'item3' => false,
        ];

        $data = [
            'arrayList' => $customArrayList,
            'manager' => $this->manager,
            'name' => 'Alex',
        ];

        $newTestObject = $this->testObjectFactory->create($data);
        $newTestObject->log();
    }
}