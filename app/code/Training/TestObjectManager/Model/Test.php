<?php

namespace Training\TestObjectManager\Model;

class Test
{
    /**
     * @var array
     */
    private $arrayList;

    /**
     * @var ManagerInterface
     */
    private $manager;
    /**
     * @var string|string
     */
    private $name;

    /**
     * @var int|int
     */
    private $number;

    /**
     * @var ManagerInterfaceFactory
     */
    private $managerFactory;

    /**
     * Test constructor.
     * @param ManagerInterface $managerInterface
     * @param array $arrayList
     * @param string $name
     * @param int $number
     * @param ManagerInterfaceFactory $managerInterfaceFactory
     */
    public function __construct(
        ManagerInterface $manager,
        array $arrayList,
        string $name,
        int $number,
        ManagerInterfaceFactory $managerFactory
    )
    {
        $this->manager = $manager;
        $this->arrayList = $arrayList;
        $this->name = $name;
        $this->number = $number;
        $this->managerFactory = $managerFactory;
    }

    public function log()
    {
        print_r(get_class($this->manager));
        echo '<br>';
        print_r($this->arrayList);
        echo '<br>';
        print_r($this->name);
        echo '<br>';
        print_r($this->number);
        echo '<br>';
        $newManager = $this->managerFactory;
        $newManagerCreate = $newManager->create();
        print_r($newManagerCreate);
    }
}