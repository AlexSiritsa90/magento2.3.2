<?php

namespace Training\TestObjectManager\Model;

class ManagerCustom implements ManagerInterface
{
    public function create()
    {
        // something create
    }

    public function get()
    {
        // something get
    }
}