define([
    'uiComponent',
    'jquery',
    'ko'
], function (Component, $, ko) {
    'use strict';

    return Component.extend({
        defaults: {
            url: '',
            currentReviewId: null
        },
        reviewerName: ko.observable(''),
        reviewerMessage: ko.observable(''),
        isLoading: ko.observable(false),

        initialize: function () {
            this._super();
            this.nextReview();
            return this;
        },
        nextReview: function () {
            this.isLoading(true);
            var self = this;

            var currentReviewId = {'currentReviewId': self.currentReviewId};

            $.ajax({
                url: self.url,
                type: 'post',
                data: currentReviewId,
                dataType: 'json'
            }).done(function (data) {
                if (data.name && data.message && data.id) {
                    self.currentReviewId = data.id;
                    self.reviewerName(data.name);
                    self.reviewerMessage(data.message);
                }
            }).always(function () {
                self.isLoading(false);
            });
        }
    });
});