define([
    'jquery',
    'mage/translate',
    'underscore',
    'Magento_Catalog/js/product/view/product-ids-resolver',
    'jquery/ui'
], function ($, $t, _, idsResolver) {
    'use strict';

    return function (widget) {
        $.widget('mage.catalogAddToCart', widget, {
            submitForm: function (form) {
                if (confirm($t('Are you sure?'))) {
                    this._super(form);
                }

                return false;
            }
        });

        return $.mage.catalogAddToCart;
    }
});
