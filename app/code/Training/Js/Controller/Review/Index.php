<?php

namespace Training\Js\Controller\Review;

use \Magento\Framework\Exception\NoSuchEntityException;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $reviewModel;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Training\Js\Model\Review $reviewModel
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->reviewModel = $reviewModel;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $currentReviewId = $this->_request->getParam('currentReviewId') ?: null;

        $resultJson->setData($this->reviewModel->getRandomReviewData($currentReviewId));
        return $resultJson;
    }
}